package com.thompson.designpatterns.structural.proxy.virtual;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.thompson.designpatterns.structural.proxy.virtual.ProxyImage;

public class VirtualProxyDemoTest {
	
	@Test
	public void testNonCachedImage(){
		ProxyImage proxyImage = new ProxyImage("TestImg.png");
		assertNull(proxyImage.getRealImage());
	}

	@Test
	public void testCachedImage(){
		ProxyImage proxyImage = new ProxyImage("TestImg.png");
		proxyImage.display();
		assertNotNull(proxyImage.getRealImage());
	}
	
}
