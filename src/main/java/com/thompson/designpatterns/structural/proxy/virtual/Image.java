package com.thompson.designpatterns.structural.proxy.virtual;

public interface Image {
	
	/**
	 * Displays an image.
	 */
	void display();

}
