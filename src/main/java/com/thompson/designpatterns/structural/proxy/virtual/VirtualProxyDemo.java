package com.thompson.designpatterns.structural.proxy.virtual;
/**
 * Virtual proxy pattern uses a representative of an object that is expensive to create in terms of memory or retrieval, this works well when creating caches.
 * @author Stephen Thompson
 */
public class VirtualProxyDemo {

	public static void main(String[] args) {
		Image image = new ProxyImage("TestImage1.png");
		
		//Before cached, first time requesting the resource.
		System.out.println("--- Before cache ---");
		image.display();

		System.out.println("");
		System.out.println("--- After cache ---");
		//After cache, no need to read from disk.
		image.display();

	}

}
