package com.thompson.designpatterns.structural.proxy.virtual;

public class ProxyImage implements Image {
	
	private RealImage realImage;
	private String fileName;
	
	public ProxyImage(String fileName){
		this.fileName = fileName;
	}
	
	public RealImage getRealImage() {
		return realImage;
	}
	
	public void display() {
		if(this.realImage == null){
			this.realImage = new RealImage(this.fileName);
		}
		realImage.display();
	}
}
