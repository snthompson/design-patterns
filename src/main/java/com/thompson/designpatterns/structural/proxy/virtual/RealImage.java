package com.thompson.designpatterns.structural.proxy.virtual;

public class RealImage implements Image {

	private String fileName;
	
	public RealImage(String fileName){
		this.fileName = fileName;
		loadFromDisk(fileName);
	}
	
	public void display() {
		System.out.println(this.fileName);
	}
	
	private void loadFromDisk(String fileName){
		System.out.println("Loading " + fileName);
	}

}
